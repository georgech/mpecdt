Computational Lab: Dynamics 2
=============================

This is the tutorial material for Computational Lab 2 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about the computation and quantification of
diverging trajectories in chaotic systems.

We are going to continue to use the Lorenz system as an example, given by

.. math::

   \begin{eqnarray}
   \dot{x} &= \sigma (y-x)\\
   \dot{y} &= x(\rho-z) - y  \\ 
   \dot{z} &= xy - \beta z
   \end{eqnarray}

and parameter values :math:`\sigma = 10,\,\rho = 28`, and :math:`\beta = 8/3`.

Exercise 1
----------

.. container:: tasks  

   Using the function from the Dynamics 1 Computational Lab, perturb
   the initial conditions by 1%. Compare the trajectories before and
   after the perturbation.  Plot the distance between the two
   trajectories as a function of time, using a semi-log graph
   (`pylab.semilogy`). Can you identify a region of exponentially-fast
   separation?

Exercise 2
----------

We can use the linearisation of the Lorenz system around a trajectory
to study this exponential separation rate. 

.. container:: tasks  

   Starting from a solution
   :math:`(x(t),y(t),z(t))` of the Lorenz equations, substitute the
   perturbed solution 
   :math:`(x(t)+\delta x(t),y(t)+\delta y(t),z(t)+\delta z(t))` into
   the equations, neglecting terms that are quadratic or higher in the 
   small perturbations :math:`(\delta x(t),\delta y(t),\delta z(t))`.
   Hence, derive a system of linear ODEs in the form

   .. math::

      \frac{d}{dt}
      \begin{pmatrix}
      \delta{x} \\
      \delta{y} \\
      \delta{z} \\
      \end{pmatrix}
      =
      A(t)   
      \begin{pmatrix}
      \delta{x} \\
      \delta{y} \\
      \delta{z} \\
      \end{pmatrix},

   where :math:`A(t)` is a time-varying 3x3 matrix which is written in
   terms of :math:`(x(t),y(t),z(t))`. You need to find the explicit
   form of :math:`A(t)`.

Exercise 3
----------

The equation for the perturbations :math:`(\delta x(t),\delta
y(t),\delta z(t))` is parameterised by the solution reference
:math:`(x(t),y(t),z(t))`, so it is easiest to solve for the reference
solution and the perturbations simultaneously. 

In the file `mpecdt/src/lyapunov.py`, the function `Phi` takes
initial conditions :math:`(x,y,z)` for the reference solution, and
initial conditions :math:`(\delta x,\delta y,\delta z)` for the
perturbation.

.. container:: tasks  

   Edit the function `vectorfield` in this file so that we
   can solve the Lorenz equation for the reference solution and the
   linearised equation for the perturbations simultaneously.

   Using the same initial condition as for Computational Lab 1,
   experiment with different initial conditions for the
   perturbations. For large times, the magnitude of the perturbation
   grows exponentially, i.e. proportional to :math:`\exp(\lambda t)`.
   The growth rate :math:`\lambda` is called the Lyaponov Exponent, which
   characterises the exponential separation rate of nearby solutions of a
   dynamical system. 

   Compare the value of :math:`\lambda` with your semilogy plot. Does the
   exponential growth region have a growth rate of :math:`\lambda`? (It
   should do!).

   Verify that the Lyaponov exponent is independent of the choice of
   initial condition for the reference solution. It is in fact an
   invariant quantity for the dynamical system.

Given initial conditions :math:`(x,y,z)` for the reference solution,
and a choice of time :math:`T`, we can define a map

.. math::
   \Phi(\delta x,\delta y,\delta z;x,y,z,T)

that maps the initial conditions for the perturbations to their final
values at time :math:`T`. 

It can be verified that this is a linear map (do it!). The largest
eigenvalue of this map is then equal to :math:`\exp(\lambda T)`. Any
initial condition for the perturbations can then be expanded in the
basis formed from the eigenvectors of :math:`\Phi`, and we see that if
we take an initial condition which has any non-zero coefficient
corresponding to the eigenvector with the largest eigenvalue, the solution
will eventually grow exponentially with rate :math:`\lambda`.

The Lyapunov exponent :math:`\lambda` describes the exponential growth
rate of a vector under the map :math:`\Phi`. We can further
characterise the dynamical system by calling this the First Lyapunov
Exponent :math:`\lambda_1`, and more generally defining the growth
rate corresponding all the eigenvalues of :math:`\Phi` as
:math:`\lambda_1,\,\lambda_2,\,\ldots,\lambda_p` in decreasing order.

Kaplan and Yorke (1979) defined a fractal dimension (Lyapunov
dimension) for a chaotic attractor by

.. math::
   D_L = j - \frac{\sum_{i=1}^j \lambda_j}{\lambda_{j+1}}

where :math:`j` is the largest integer such that 

.. math::
   \sum_{i=1}^j \lambda_i > 0.

They proved that this fractal dimension is a lower bound for the
box-counting dimension of the attractor. 

Exercise 4 - Advanced topic
----------

Computing higher Lyapunov exponents is more tricky, as the problem is
very sensitive to errors. One practical approach was suggested by
Bennetin et al (1980). 

We start by defining Lyapunov exponents of order :math:`p`,
:math:`\lambda^p`, as the exponential growth rate of the
:math:`p` -dimensional volume of the parallelopiped formed by :math:`p`
linearly independent perturbation vectors
:math:`(u_1,u_2,\ldots,u_p)`, where

.. math::
   u_k = (\delta x_k(t),\delta y_k(t),\delta z_k(t)), \quad k=1,2,\ldots,p,

and :math:`(\delta x_k(t),\delta y_k(t),\delta z_k(t))` solves the 
perturbation equation. The Lyaponov exponents we discussed in previous
exercises are Lyapunov exponents of order 1.

Oselec (1968) proved that 

.. math::
   \lambda^p = \lambda_1 + \ldots \lambda_p,

i.e., the Lypunov exponents of order :math:`p` can be related to each
of the Lyapunov exponents of order 1.

- Choose an initial condition :math:`X^0 = (x^0,y^0,z^0)`.

- Starting from a set :math:`(u_1^0,\ldots,u_p^0)` of :math:`p` linearly
  independent vectors, the Gram-Schmidt orthonormalisation procedure is

  .. math::
     \begin{split}
     w^0_1 = u^0_1, \, v^0_1 = w^0_1/\|w^0_1\|, \\
     w^0_2 = u^0_2, - (u^0_2\cdot v^0_1)v^0_1,\, v^0_2  = w^0_2/\|w^0_2\|, \\
     \ldots \\
     w^0_p = u^0_p - \sum_{i=1}^{p-1}(u^0_p\cdot v^0_i)v^0_i, \,
     v^0_p = w^0_p/\|w^0_p\|.
     \end{split}

  This produces an orthonormal basis
  :math:`(v^0_1,\ldots,v^0_p)` for the subspace spanned by
  :math:`(u^0_1,\ldots,u^0_p)`, and the volume of the parallelopiped spanned
  by :math:`(u^0_1,\ldots,u^0_p)` is given by

  .. math::
     Vol(u^0_1,\ldots,u^0_p) = \prod_{i=1}^p \|w^0_i\|.

- We then solve the linearised equations for a short interval
  :math:`\Delta t`, with initial condition :math:`X^0` for the reference
  solution and normalised perturbation vectors :math:`v^0_k`, for each
  :math:`k=1,\ldots,p`, to obtain a new set of vectors
  :math:`(u_1^1,\ldots,u_p^1)`, together with the value :math:`X^1` 
  for the reference solution.

- We then repeat the Gram-Schmidt procedure to obtain a new weighted
  orthonormal set :math:`\{w^1_i\}_{i=1}^p`, use it to compute 
  :math:`Vol(u^1_1,\ldots,u^1_p)`, and then solve the linearised
  equations forward in time by :math:`\Delta t` with initial 
  condition :math:`X^1` for the reference solution, and normalised
  perturbation vectors :math:`v^0_k`, for each
  :math:`k=1,\ldots,p`, to obtain a new set of vectors
  :math:`(u_2^1,\ldots,u_p^2)`, together with the value :math:`X^2` 
  for the reference solution.

- This is iterated many times.

Hence, we can compute the Lyapunov exponents of order p from

.. math::
   \lambda^p = \lim_{k\to\infty}\frac{1}{k\Delta T}\sum_{i=1}^k
   \ln\left(\|w_1^i\|\|w_2^i\|\ldots\|w^i_p\|\right).

Subtracting :math:`\lambda^{p-1}` from :math:`\lambda^p` and using
Oselec's result, we get

.. math::

   \lambda_p = \lim_{k\to \infty}\frac{1}{K\Delta t}
   \sum_{i=1}^k\ln\|w_p^i\|.

This suggests a numerical method to compute :math:`\lambda_p` by
truncating this sum once convergence to a particular tolerance is
reached.

.. container:: tasks  

   Implement this procedure to compute the Lyapunov spectrum, and hence
   the Lyapunov dimension of the Lorenz attractor. You need to implement
   the Gram-Schmidt algorithm for :math:`p=3`, or use the QR
   factorisation from `scipy.linalg.qr` if you know how to relate that
   back to Gram-Schmidt.

   Add this code to the `lyapunov.py` and push it up to BitBucket.

References
----------

- Benettin, G., et al. "Lyapunov characteristic exponents for smooth
  dynamical systems; a method for computing all of them. Part 2:
  Numerical application." Meccanica 15 (1980): 21-30.
- Kaplan, J. L., and J. A. Yorke. "Functional differential equations
  and approximation of fixed points." Lecture Notes in Mathematics 730
  (1979): 204.
- Oseledec, V. "A multiplicative ergodic
  theorem. Lyapunov characteristic numbers for dynamical systems."
  Trans. Moscow Math. Soc 19.2 (1968): 197-231.
