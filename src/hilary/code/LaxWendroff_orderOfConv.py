from __future__ import absolute_import, division, print_function

### The sys module provides access to operating system commands and ###
### access to command line variables                                ###
import sys

import matplotlib.pyplot as plt

from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

import math as mth

c=0.2
cosine = ( lambda x: np.cos(2*np.pi*x) )
l2_errors = []
dx_values = []
step_size = 100
for i in range(1,10):
    nx = step_size*i
    nt = nx
    grid = Grid(nx, 0, 1)
    phiExact = cosine((grid.x - c*nt*grid.dx)%grid.length)
    phiOld = cosine(grid.x)
    dx_values.append(grid.dx)
    l2_errors.append(errorDiagnostics(grid, LaxWendroff(phiOld.copy(), c, nt), phiExact).l2())
plt.figure()
plt.plot(dx_values, l2_errors)
plt.xlabel('$\Delta x$ ($=\Delta t$)')
plt.ylabel('$l^2$ Error')
plt.show()

plt.figure()
plt.plot(dx_values, l2_errors)
slope = np.polyfit(map(lambda x: mth.log(x), dx_values), map(lambda x: mth.log(x), l2_errors), 1)[0]
print("\n\n Slope: " + str(slope))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$log(\Delta x) (=log(\Delta x))$')
plt.ylabel('$log$ of $l^2$ Error')
plt.show()

