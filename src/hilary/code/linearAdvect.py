#!/usr/bin/python

# Outer code for setting up the linear advection problem on a uniform
# grid and calling the function to perform the linear advection and plot.
# The function takes an argument which gives the name of the input file


from __future__ import absolute_import, division, print_function

### The sys module provides access to operating system commands and ###
### access to command line variables                                ###
import sys

import matplotlib.pyplot as plt

# read in all the linear advection schemes, initial conditions and other
# code associated with this application
from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

import math as mth

def main(argv):
    ### Test the command line argument and from these set the       ###
    ### name of the input file.                                     ###
    if len(sys.argv) != 2:
        print('usage: linearAdvect <inputFile.py>')
        sys.exit(2)
    inputFile = sys.argv[1]
    print('Reading input from file ', inputFile)

    # Define input variables from the input file and store in dictionary 
    # inputs
    inputs={}
    execfile(inputFile, inputs)
    
    print('Advecting the profile ...')

    ### Declare an instance of the Grid class, called grid which    ###
    ### defines the grid for these simulations. Thus grid.dx and    ###
    ### grid.x will automatically be defined
    grid = Grid(inputs['nx'], inputs['xmin'], inputs['xmax'])

    # Other input variables
    c = inputs['c']                # Courant number for the advection
    nt = inputs['nt']              # Total number of time steps to run for
    # Select which initial conditions to use (from initialConditions.py)
    initialConditions = eval(inputs['initialConditions'])
    
    # Initialise dependent variable
    phiOld = initialConditions(grid.x)
    # Exact solution is the initial condition shifted around the domain
    phiExact = initialConditions((grid.x - c*nt*grid.dx)%grid.length)

    # Advect the profile using finite difference for all the time steps
    phiFTCS = FTCS(phiOld.copy(), c, nt)
    phiFTBS = FTBS(phiOld.copy(), c, nt)
    phiCTCS = CTCS(phiOld.copy(), c, nt)
    phiBTCS = BTCS(phiOld.copy(), c, nt)
    phiLaxWendroff = LaxWendroff(phiOld.copy(), c, nt)
    
    # calculate the error norms, mean and standard deviation
    noErrors = errorDiagnostics(grid, phiExact, phiExact)
    noErrors.write("Exact")
    FTCSerrors = errorDiagnostics(grid, phiFTCS, phiExact)
    FTCSerrors.write("FTCS")
    FTBSerrors = errorDiagnostics(grid, phiFTBS, phiExact)
    FTBSerrors.write("FTBS")
    CTCSerrors = errorDiagnostics(grid, phiCTCS, phiExact)
    CTCSerrors.write("CTCS")
    BTCSerrors = errorDiagnostics(grid, phiBTCS, phiExact)
    BTCSerrors.write("BTCS")
    LaxWendroffErrors = errorDiagnostics(grid, phiLaxWendroff, phiExact)
    LaxWendroffErrors.write("Lax Wendroff")

    plotFinal(grid.x, [phiExact, phiFTCS, phiFTBS, phiCTCS, phiBTCS, phiLaxWendroff], 
              ['Exact Solution', 'FTCS', 'FTBS', 'CTCS', 'BTCS', 'LaxWendroff'], "graphs")

    ### Create plot with two curves and two labels and save figure  ###
    ### to the outFile                                              ###
    #plotFinal(grid.x, [phiExact, phiFTCS],
    #          ['Exact Solution', 'FTCS'], inputs['outFile'])

    #plotFinal(grid.x, [phiExact, phiFTBS],
    #          ['Exact Solution', 'FTBS'], inputs['outFile'])

    #plotFinal(grid.x, [phiExact, phiCTCS],
    #          ['Exact Solution', 'CTCS'], inputs['outFile'])

    #plotFinal(grid.x, [phiExact, phiBTCS],
    #          ['Exact Solution', 'BTCS'], inputs['outFile'])

    plotFinal(grid.x, [phiExact, phiLaxWendroff], 
              ['Exact Solution', 'Lax Wendroff'], inputs['outFile'])

    #plotErrors(CTCS,phiOld.copy(),c,grid)

    #orderOfConv(0.2)


def orderOfConv(c):
    cosine = ( lambda x: np.cos(2*np.pi*x) )
    l2_errors = []
    dx_values = []
    step_size = 1000
    for i in range(1,10):
        nx = step_size*i
        nt = nx
        grid = Grid(nx, 0, 1)
        phiExact = cosine((grid.x - c*nt*grid.dx)%grid.length)
        phiOld = cosine(grid.x)
        dx_values.append(grid.dx)
        l2_errors.append(errorDiagnostics(grid, CTCS(phiOld.copy(), c, nt), phiExact).l2())
    plt.figure()
    plt.plot(dx_values, l2_errors)
    plt.xlabel('$\Delta x$')
    plt.ylabel('$l^2$ error')
    plt.show()

    plt.figure()
    plt.plot(dx_values, l2_errors)
    slope = np.polyfit(map(lambda x: mth.log(x), dx_values), map(lambda x: mth.log(x), l2_errors), 1)[0]
    print("\n\n Slope: " + str(slope))
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('$log(\Delta x)$')
    plt.ylabel('$log$ of $l^2$ error')
    plt.show()



def plotErrors(advectionScheme, phiOld, c, grid):
    l2_errors = []
    linf_errors = []
    step_size = 2000
    nt_values = range(1,30)
    for ts in nt_values:
        nt = ts*step_size
        phiExact = cosBell((grid.x - c*nt*grid.dx)%grid.length)
        errorResults = errorDiagnostics(grid, advectionScheme(phiOld.copy(), c, nt), phiExact)
        l2_errors.append(errorResults.l2())
        linf_errors.append(errorResults.linf)
    plt.plot(map((lambda x : step_size*x), nt_values), l2_errors, label='$l^2$')
    plt.plot(map((lambda x : step_size*x), nt_values), linf_errors, label='$l^{\infty}$')
    plt.xlabel('Number of time steps taken')
    plt.ylabel('Error')
    plt.grid(True)
    plt.legend(loc='best')
    graph_title = str(advectionScheme).split()
    plt.title(graph_title[1])
    plt.show()



### Run the function main defined in this file with one argument    ###
if __name__ == "__main__":
   main(sys.argv[1:])

