# Assignment 2 question 4

from __future__ import absolute_import, division, print_function

from diagnostics import *
from initialConditions import *
from grid import *


import numpy as np
import matplotlib.pyplot as plt


def mean(phi, grid):
    return errorDiagnostics(grid, phi, phi).mean(phi)

def std_deviation(phi, grid):
    return errorDiagnostics(grid, phi, phi).stdDev(phi)


nx = 100
nt = 10000
c = 0.2
xmin = 0
xmax = 1


grid = Grid(nx, xmin, xmax)
phiOld = cosBell(grid.x) #can put cosFctn

init_mean = mean(phiOld, grid)
init_std_dev = std_deviation(phiOld, grid)

mean_array = []
std_dev_array = []

# Lax Wendroff starts here
phi = np.zeros_like(phiOld)

for it in range(nt):
    for j in range(nx):
        phi[j] = phiOld[j] - 0.5*c*\
                  (2*c*phiOld[j] + (1-c)*phiOld[(j+1)%nx] -\
                  (1+c)*phiOld[(j-1)%nx])

    phiOld = phi.copy()
    mean_array.append(mean(phi, grid))
    std_dev_array.append(std_deviation(phi, grid))
# Lax Wendroff ends here

mean_array = map((lambda x: (x - init_mean)/init_mean), mean_array)

std_dev_array = map((lambda x: (x - init_std_dev)/init_std_dev), std_dev_array)

plt.plot(range(len(mean_array)), mean_array)
plt.xlabel('Time step')
plt.ylabel('Normalised Mean')
#plt.ylim(-1,1)
plt.show()

plt.plot(range(len(std_dev_array)), std_dev_array)
plt.xlabel('Time step')
plt.ylabel('Normalised Standard deviation')
#plt.ylim(-1,1)
plt.show()

