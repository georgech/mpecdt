from __future__ import absolute_import, division, print_function
import numpy as np

# Various different initial conditions for linear advection

def cosBell(x):
    "Function defining a cosine bell as a function of position, x"
    bell = lambda x: 0.5*(1 - np.cos(4*np.pi*x))
    return np.where((x<0.5) | (x>=1.0), bell(x), 0.)

def squareWave(x):
    "Function defining a square wave as a function of position, x"
    return np.where((x<0.5) | (x>=1.0), 1., 0.)

def mixed(x):
    "A flat peak in one location and a cosine bell in another with"
    "ramps up to and down from the flat peak"
    return np.where((x >= 0.2) & (x <= 0.3), 1, \
                    np.where((x >= 0.4) & (x <= 0.8), \
                    0.5*(1 + np.cos(5*np.pi*(x-0.6))),
                    np.where((x > 0.1) & (x < 0.2), 10*(x-0.1), 
                    np.where((x > 0.3) & (x < 0.35), 20*(0.35-x), 0))))

def cosFctn(x):
    "The initial condition function for assignment 2"
    cosFct = lambda x: 0.5*(1 + np.cos(5*np.pi*(x-0.6)))
    return np.where((x >= 0.2) & (x < 0.3), 1, \
                    np.where(((x >= 0.4) & (x < 0.8)), cosFct(x), 0))

