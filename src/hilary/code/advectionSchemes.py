# Numerical schemes for simulating linear advection for outer code
# linearAdvect.py 
from __future__ import absolute_import, division, print_function
import numpy as np
from diagnostics import *
import scipy.linalg as la


def FTCS(phiOld, c, nt):
    "Linear advection of profile in phiOld using FTCS, Courant number c"
    "for nt time-steps"
    
    nx = len(phiOld)

    # plot results as we go along
    #plotSolution(phiOld, "Initial Conditions")

    # new time-step array for phi
    phi = np.zeros_like(phiOld)

    # FTCS for each time-step
    for it in range(nt):
        # Loop through all space using remainder after division (%)
        # to cope with periodic boundary conditions
        for j in range(nx):
            phi[j] = phiOld[j] - 0.5*c*\
                     (phiOld[(j+1)%nx] - phiOld[(j-1)%nx])
        
        # update arrays for next time-step
        phiOld = phi.copy()

        # plot for this time-step
        #plotSolution(phi, "FTCS, time step "+str(it))
    
    return phi


def FTBS(phiOld, c, nt):
    "Linear advection of profile in phiOld using FTBS, Courant number c"
    "for nt time-steps"

    nx = len(phiOld)

    #plotSolution(phiOld, "Initial Conditions")

    phi = np.zeros_like(phiOld)

    for it in range(nt):
        for j in range(nx):
            phi[j] = phiOld[j] - c*(phiOld[j] - phiOld[(j-1)%nx])

        phiOld = phi.copy()
        #plotSolution(phi, "FTBS, time step " + str(it))

    return phi


def CTCS(phiOld, c, nt):
    "Linear advection of profile in phiOld using CTCS, Courant number c"
    "for nt time-steps"

    nx = len(phiOld)

    #plotSolution(phiOld, "Initial Conditions")

    # Run one iteration of FTCS in order to get values after one time step.
    # This is needed because at each time step, CTCS uses values at the
    # previous two time steps.
    phi = np.zeros_like(phiOld)
    for j in range(nx):
        phi[j] = phiOld[j] - 0.5*c*(phiOld[(j+1)%nx] - phiOld[(j-1)%nx])

    phiNew = np.zeros_like(phiOld)
    #plotSolution(phiNew, "CTCS, time step " + str(0))

    for it in range(nt-1):
        for j in range(nx):
            phiNew[j] = phiOld[j] - c*(phi[(j+1)%nx] - phi[(j-1)%nx])

        phiOld = phi.copy()
        phi = phiNew.copy()
        #plotSolution(phiNew, "CTCS, time step " + str(it+1))

    return phiNew


def BTCS(phiOld, c, nt):
    "Linear advection of profile in phiOld using BTCS, Courant number c"
    "for nt time-steps"

    nx = len(phiOld)

    #plotSolution(phiOld, "Initial Conditions")

    # Initialise the coefficient matrix
    M = np.zeros([nx,nx])
    for i in range(nx):
        M[i,i] = 1
        M[i,(i+1)%nx] = 0.5*c
        M[i,(i-1)%nx] = -0.5*c

    phi = np.zeros_like(phiOld)

    for it in range(nt):
        # Solve the matrix equation: M * phi = phiOld
        phi = la.solve(M, phiOld)

        phiOld = phi.copy()
        #plotSolution(phi, "BTCS, time step " + str(it))

    return phi


def LaxWendroff(phiOld, c, nt):
    "Lax Wendroff finite volume advection scheme for profile in phiOld"
    "and Courant number c, for nt time-steps"

    nx = len(phiOld)

    #plotSolution(phiOld, "Initial Conditions")

    # new time-step array for phi
    phi = np.zeros_like(phiOld)

    # For each time-step it:
    for it in range(nt):
        # Loop through all space using remainder after division (%)
        # to cope with periodic boundary conditions
        for j in range(nx):
            phi[j] = phiOld[j] - 0.5*c*\
                     (2*c*phiOld[j] + (1-c)*phiOld[(j+1)%nx] -\
                      (1+c)*phiOld[(j-1)%nx])

        # update arrays for next time-step
        phiOld = phi.copy()

        # plot for this time-step
        #plotSolution(phi, "Lax-Wendroff, time step "+str(it))
    
    return phi

