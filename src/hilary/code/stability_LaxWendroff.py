# Assignment 3 - stability investigation of Lax Wendroff

from __future__ import absolute_import, division, print_function

from diagnostics import *
from initialConditions import *
from grid import *

import numpy as np
import matplotlib.pyplot as plt


def l2Error(phi, phiExact, grid):
    return errorDiagnostics(grid, phi, phiExact).l2()

def linfError(phi, phiExact, grid):
    return errorDiagnostics(grid, phi, phiExact).linf



nx = 100
nt = 2000000
c = -0.9
xmin = 0
xmax = 1

grid = Grid(nx, xmin, xmax)
#phiExact = cosBell((grid.x - c*nt*grid.dx)%grid.length)
phiOld = cosBell(grid.x)

l2_errors = []
linf_errors = []


phi = np.zeros_like(phiOld)

for it in range(nt):
    for j in range(nx):
        phi[j] = phiOld[j] - 0.5*c*\
                     (2*c*phiOld[j] + (1-c)*phiOld[(j+1)%nx] -\
                      (1+c)*phiOld[(j-1)%nx])

    phiOld = phi.copy()

    phiExact = cosBell((grid.x - c*it*grid.dx)%grid.length)
    l2_errors.append(l2Error(phi, phiExact, grid))
    linf_errors.append(linfError(phi, phiExact, grid))


plt.plot(range(nt), l2_errors, label='$l^2$')
plt.plot(range(nt), linf_errors, label='$l^{\infty}$')
plt.xlabel('Time Step')
plt.ylabel('Error')
plt.grid(True)
plt.legend(loc='best')
plt.title('Lax Wendroff with Courant number $c = ' + str(c) + '$')
plt.show()

